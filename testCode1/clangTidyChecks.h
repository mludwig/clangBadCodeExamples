/*
 * clangTidyChecks.h
 *
 *  Created on: Sep 16, 2019
 *      Author: mludwig
 */

#ifndef TESTCODE1_CLANGTIDYCHECKS_H_
#define TESTCODE1_CLANGTIDYCHECKS_H_

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>     // std::cout
#include <functional>   // std::minus
#include <numeric>      // std::accumulate
#include <string.h>

class clangTidyChecks {
public:
	clangTidyChecks();
	virtual ~clangTidyChecks();

	int bugproneDynamicStaticInitializers();
	void bugproneFoldInitType();
	void unixCstringBadsizeArg();
};



// https://clang.llvm.org/extra/clang-tidy/checks/bugprone-virtual-near-miss.html
struct Base {
  virtual void func();
  virtual void ~func();
};

struct Derived : Base {
  virtual funk();
  // warning: 'Derived::funk' has a similar name and the same signature as virtual method 'Base::func'; did you mean to override it?
};


#endif /* TESTCODE1_CLANGTIDYCHECKS_H_ */
