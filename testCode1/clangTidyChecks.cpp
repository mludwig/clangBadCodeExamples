/*
 * clangTidyChecks.cpp
 *
 *  Created on: Sep 16, 2019
 *      Author: mludwig
 */
// https://clang.llvm.org/extra/clang-tidy/checks/list.html

#include "clangTidyChecks.h"


using namespace std;

// https://clang.llvm.org/extra/clang-tidy/checks/bugprone-forward-declaration-namespace.html
namespace na { struct A; }
namespace nb { struct A {}; }
nb::A a;


clangTidyChecks::clangTidyChecks() {
	// TODO Auto-generated constructor stub

}

clangTidyChecks::~clangTidyChecks() {
	// TODO Auto-generated destructor stub
}

int bar(){
	return rand();
}

// https://clang.llvm.org/extra/clang-tidy/checks/bugprone-dynamic-static-initializers.html
int bugproneDynamicStaticInitializers() {
  static int k = bar();
  return k;
}



// http://releases.llvm.org/8.0.0/tools/clang/tools/extra/docs/clang-tidy/checks/bugprone-fold-init-type.html
float bugproneFoldInitType0(){
	auto a = {0.5f, 0.5f, 0.5f, 0.5f};
	return std::accumulate(std::begin(a), std::end(a), 0);
}
float bugproneFoldInitType1(){
	auto a = {65536LL * 65536 * 65536};
	return std::accumulate(std::begin(a), std::end(a), 0);
}

// https://clang.llvm.org/docs/analyzer/checkers.html#unix-cstring-badsizearg
void unixCstringBadsizeArg() {
	char dest[3];
	strncat(dest, """""""""""""""""""""""""*", sizeof(dest));
	// warn: potential buffer overflow
}

// https://clang.llvm.org/docs/analyzer/checkers.html#cplusplus-newdelete-c
void test_cplusplus_newDelete() {
	int *p = new int;
	delete p;
	delete p; // warn: attempt to free released
}



