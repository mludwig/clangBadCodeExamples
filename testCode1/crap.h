/*
 * crap.h
 *
 *  Created on: Sep 12, 2019
 *      Author: mludwig
 */

#ifndef TESTCODE1_CRAP_H_
#define TESTCODE1_CRAP_H_

class crap {
public:
	crap();
	virtual ~crap();

	int myvar1 = 0;
	void doSth1( void ){};

private:
	// private vars should be prefixed with _
	int myvar0 = 0;

	// private methods should be prefixed with _
	void doSth0( void ){};
};

#endif /* TESTCODE1_CRAP_H_ */
