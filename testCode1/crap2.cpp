// crap2.cpp
// bad code for testing some basic stuff with code checkers such as cppcheck, clang-tidy


#include <stdio.h>
#include <stdlib.h>

using namespace std;

// warning: namespace 'i' not terminated with a closing comment [llvm-namespace-comment]
namespace i {}


int myxxxmain ( int argc, char ** argv ){


	// mem hole
	int *p = (int *) calloc( 10, 100 );


	bool bb = true;
	int *ptr = bb; // wtf assign bool to ptr

	// type cast truncates
	int i = 1.99;

	// password hardcoded
	string password = "admin";

	// wtf goto is not allowed
	goto label;
    label:

	// wtf backward goto is even worse
	label1:
	goto label1;

    // wtf label dont exist
	goto nullabel;



	switch( 1 ){
	default:{
		break;
		break; // superfluous
	}
	}

	// no return value
}


