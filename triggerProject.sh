#!/bin/bash
# commit a fake change to repo and thus trigger a pipeline
#  we just add a line to triggers.txt
rm -f ./triggers.txt 
touch ./triggers.txt
echo "triggered at "`date` | tee ./triggers.txt
git add ./triggers.txt
git commit -m "triggered at `date`"
git push origin master


